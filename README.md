## AMI Metadata
Schema and samples of the metadata at each stage of the AMI digitization workflow prior to Media Ingest.

* Shipment - metadata that accompanies media going to digitization
* Vendor - metadata that accompanies media coming from digitization
* QC - metadata that accompanies media after quality control

### Running Schema Validations/Tests

We've used [ajv-cli](https://github.com/jessedc/ajv-cli) to test.

e.g: `ajv validate -s schema/digitized.json -r "schema/*.json" -d "sample/*.json"`
